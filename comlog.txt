[262D[4C[Kp[262D[4C[1C[Kr[262D[4C[2C[Ki[262D[4C[3C[Kn[262D[4C[4C[Kt[262D[4C[5C[K [262D[4C[6C[K/[262D[4C[7C[Ka[262D[4C[8C[Kb[262D[4C[9C[Ko[262D[4C[10C[Ku[262D[4C[11C[Kt[262D[4C[12C
{
	"device": "Joy5",
	"fw_version": 0
}[262D[4C[Kp[262D[4C[1C[Kr[262D[4C[2C[Ki[262D[4C[3C[Kn[262D[4C[4C[Kt[262D[4C[5C[K [262D[4C[6C[K/[262D[4C[7C[Ka[262D[4C[8C[Kb[262D[4C[9C[Ko[262D[4C[10C[Ku[262D[4C[11C[Kt[262D[4C[12C
{
	"device": "Joy5",
	"fw_version": 0
}
j5> [262D[4C[Kp[262D[4C[1C[Kr[262D[4C[2C[Ki[262D[4C[3C[Kn[262D[4C[4C[Kt[262D[4C[5C[K [262D[4C[6C[K/[262D[4C[7C[Ks[262D[4C[8C[Ke[262D[4C[9C[Kt[262D[4C[10C[Kt[262D[4C[11C[Ki[262D[4C[12C[Kn[262D[4C[13C[Kg[262D[4C[14C[Ks[262D[4C[15C
{
	"sticks": [
		{
			"calibration": {
				"sensorMin": {
					"x": 0.150238,
					"y": 0.198059
				},
				"sensorMax": {
					"x": 0.838501,
					"y": 0.886169
				}
			},
			"activateRadiuses": [
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000
			],
			"stickRotationDegrees": 0.000000
		},
		{
			"calibration": {
				"sensorMin": {
					"x": 0.239655,
					"y": 0.238007
				},
				"sensorMax": {
					"x": 0.859802,
					"y": 0.891449
				}
			},
			"activateRadiuses": [
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000
			],
			"stickRotationDegrees": 0.000000
		},
		{
			"calibration": {
				"sensorMin": {
					"x": 0.169952,
					"y": 0.232269
				},
				"sensorMax": {
					"x": 0.831512,
					"y": 0.855316
				}
			},
			"activateRadiuses": [
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000
			],
			"stickRotationDegrees": 0.000000
		},
		{
			"calibration": {
				"sensorMin": {
					"x": 0.228088,
					"y": 0.196625
				},
				"sensorMax": {
					"x": 0.853333,
					"y": 0.849457
				}
			},
			"activateRadiuses": [
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000
			],
			"stickRotationDegrees": 0.000000
		},
		{
			"calibration": {
				"sensorMin": {
					"x": 0.000549,
					"y": 0.000549
				},
				"sensorMax": {
					"x": 0.999634,
					"y": 0.991638
				}
			},
			"activateRadiuses": [
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000,
				0.700000
			],
			"stickRotationDegrees": 0.000000
		}
	],
	"layouts": [
		[
			[
				{
					"center_angle_3600": 0,
					"half_size_angle_3600": 225,
					"keyCode": 25
				},
				{
					"center_angle_3600": 450,
					"half_size_angle_3600": 225,
					"keyCode": 26
				},
				{
					"center_angle_3600": 900,
					"half_size_angle_3600": 225,
					"keyCode": 27
				},
				{
					"center_angle_3600": 1350,
					"half_size_angle_3600": 225,
					"keyCode": 29
				},
				{
					"center_angle_3600": 1800,
					"half_size_angle_3600": 225,
					"keyCode": 44
				},
				{
					"center_angle_3600": 2250,
					"half_size_angle_3600": 225,
					"keyCode": 44
				},
				{
					"center_angle_3600": 2700,
					"half_size_angle_3600": 225,
					"keyCode": 44
				},
				{
					"center_angle_3600": 3150,
					"half_size_angle_3600": 225,
					"keyCode": 44
				}
			],
			[
				{
					"center_angle_3600": 0,
					"half_size_angle_3600": 450,
					"keyCode": 44
				},
				{
					"center_angle_3600": 900,
					"half_size_angle_3600": 450,
					"keyCode": 536870912
				},
				{
					"center_angle_3600": 1800,
					"half_size_angle_3600": 450,
					"keyCode": 268435458
				},
				{
					"center_angle_3600": 2700,
					"half_size_angle_3600": 450,
					"keyCode": 268435457
				}
			],
			[
				{
					"center_angle_3600": 0,
					"half_size_angle_3600": 225,
					"keyCode": 15
				},
				{
					"center_angle_3600": 450,
					"half_size_angle_3600": 225,
					"keyCode": 16
				},
				{
					"center_angle_3600": 900,
					"half_size_angle_3600": 225,
					"keyCode": 17
				},
				{
					"center_angle_3600": 1350,
					"half_size_angle_3600": 225,
					"keyCode": 19
				},
				{
					"center_angle_3600": 1800,
					"half_size_angle_3600": 225,
					"keyCode": 20
				},
				{
					"center_angle_3600": 2250,
					"half_size_angle_3600": 225,
					"keyCode": 21
				},
				{
					"center_angle_3600": 2700,
					"half_size_angle_3600": 225,
					"keyCode": 22
				},
				{
					"center_angle_3600": 3150,
					"half_size_angle_3600": 225,
					"keyCode": 23
				}
			],
			[
				{
					"center_angle_3600": 0,
					"half_size_angle_3600": 225,
					"keyCode": 5
				},
				{
					"center_angle_3600": 450,
					"half_size_angle_3600": 225,
					"keyCode": 6
				},
				{
					"center_angle_3600": 900,
					"half_size_angle_3600": 225,
					"keyCode": 7
				},
				{
					"center_angle_3600": 1350,
					"half_size_angle_3600": 225,
					"keyCode": 9
				},
				{
					"center_angle_3600": 1800,
					"half_size_angle_3600": 225,
					"keyCode": 10
				},
				{
					"center_angle_3600": 2250,
					"half_size_angle_3600": 225,
					"keyCode": 11
				},
				{
					"center_angle_3600": 2700,
					"half_size_angle_3600": 225,
					"keyCode": 13
				},
				{
					"center_angle_3600": 3150,
					"half_size_angle_3600": 225,
					"keyCode": 14
				}
			],
			[
				{
					"center_angle_3600": 0,
					"half_size_angle_3600": 225,
					"keyCode": 4
				},
				{
					"center_angle_3600": 450,
					"half_size_angle_3600": 225,
					"keyCode": 8
				},
				{
					"center_angle_3600": 900,
					"half_size_angle_3600": 225,
					"keyCode": 18
				},
				{
					"center_angle_3600": 1350,
					"half_size_angle_3600": 225,
					"keyCode": 12
				},
				{
					"center_angle_3600": 1800,
					"half_size_angle_3600": 225,
					"keyCode": 24
				},
				{
					"center_angle_3600": 2250,
					"half_size_angle_3600": 225,
					"keyCode": 28
				},
				{
					"center_angle_3600": 2700,
					"half_size_angle_3600": 225,
					"keyCode": 40
				},
				{
					"center_angle_3600": 3150,
					"half_size_angle_3600": 225,
					"keyCode": 40
				}
			]
		],
		[
			[
				{
					"center_angle_3600": 0,
					"half_size_angle_3600": 225,
					"keyCode": 42
				},
				{
					"center_angle_3600": 450,
					"half_size_angle_3600": 225,
					"keyCode": 41
				},
				{
					"center_angle_3600": 900,
					"half_size_angle_3600": 225,
					"keyCode": 76
				},
				{
					"center_angle_3600": 1350,
					"half_size_angle_3600": 225,
					"keyCode": 58
				},
				{
					"center_angle_3600": 1800,
					"half_size_angle_3600": 225,
					"keyCode": 44
				},
				{
					"center_angle_3600": 2250,
					"half_size_angle_3600": 225,
					"keyCode": 44
				},
				{
					"center_angle_3600": 2700,
					"half_size_angle_3600": 225,
					"keyCode": 44
				},
				{
					"center_angle_3600": 3150,
					"half_size_angle_3600": 225,
					"keyCode": 44
				}
			],
			[
			],
			[
				{
					"center_angle_3600": 0,
					"half_size_angle_3600": 225,
					"keyCode": 39
				},
				{
					"center_angle_3600": 450,
					"half_size_angle_3600": 225,
					"keyCode": 30
				},
				{
					"center_angle_3600": 900,
					"half_size_angle_3600": 225,
					"keyCode": 31
				},
				{
					"center_angle_3600": 1350,
					"half_size_angle_3600": 225,
					"keyCode": 32
				},
				{
					"center_angle_3600": 1800,
					"half_size_angle_3600": 225,
					"keyCode": 33
				},
				{
					"center_angle_3600": 2250,
					"half_size_angle_3600": 225,
					"keyCode": 3118
				},
				{
					"center_angle_3600": 2700,
					"half_size_angle_3600": 225,
					"keyCode": 45
				},
				{
					"center_angle_3600": 3150,
					"half_size_angle_3600": 225,
					"keyCode": 3111
				}
			],
			[
				{
					"center_angle_3600": 0,
					"half_size_angle_3600": 225,
					"keyCode": 34
				},
				{
					"center_angle_3600": 450,
					"half_size_angle_3600": 225,
					"keyCode": 35
				},
				{
					"center_angle_3600": 900,
					"half_size_angle_3600": 225,
					"keyCode": 36
				},
				{
					"center_angle_3600": 1350,
					"half_size_angle_3600": 225,
					"keyCode": 37
				},
				{
					"center_angle_3600": 1800,
					"half_size_angle_3600": 225,
					"keyCode": 38
				},
				{
					"center_angle_3600": 2250,
					"half_size_angle_3600": 225,
					"keyCode": 3109
				},
				{
					"center_angle_3600": 2700,
					"half_size_angle_3600": 225,
					"keyCode": 56
				},
				{
					"center_angle_3600": 3150,
					"half_size_angle_3600": 225,
					"keyCode": 3110
				}
			],
			[
				{
					"center_angle_3600": 0,
					"half_size_angle_3600": 225,
					"keyCode": 55
				},
				{
					"center_angle_3600": 450,
					"half_size_angle_3600": 225,
					"keyCode": 54
				},
				{
					"center_angle_3600": 900,
					"half_size_angle_3600": 225,
					"keyCode": 3128
				},
				{
					"center_angle_3600": 1350,
					"half_size_angle_3600": 225,
					"keyCode": 3102
				},
				{
					"center_angle_3600": 1800,
					"half_size_angle_3600": 225,
					"keyCode": 52
				},
				{
					"center_angle_3600": 2250,
					"half_size_angle_3600": 225,
					"keyCode": 3126
				},
				{
					"center_angle_3600": 2700,
					"half_size_angle_3600": 225,
					"keyCode": 3127
				},
				{
					"center_angle_3600": 3150,
					"half_size_angle_3600": 225,
					"keyCode": 46
				}
			]
		],
		[
			[
			],
			[
			],
			[
			],
			[
			],
			[
				{
					"center_angle_3600": 0,
					"half_size_angle_3600": 225,
					"keyCode": 47
				},
				{
					"center_angle_3600": 450,
					"half_size_angle_3600": 225,
					"keyCode": 48
				},
				{
					"center_angle_3600": 900,
					"half_size_angle_3600": 225,
					"keyCode": 3119
				},
				{
					"center_angle_3600": 1350,
					"half_size_angle_3600": 225,
					"keyCode": 3120
				},
				{
					"center_angle_3600": 1800,
					"half_size_angle_3600": 225,
					"keyCode": 3117
				},
				{
					"center_angle_3600": 2250,
					"half_size_angle_3600": 225,
					"keyCode": 44
				},
				{
					"center_angle_3600": 2700,
					"half_size_angle_3600": 225,
					"keyCode": 44
				},
				{
					"center_angle_3600": 3150,
					"half_size_angle_3600": 225,
					"keyCode": 44
				}
			]
		]
	]
}