package org.joy5;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.TimerTask;

public class JApp extends Application {
    App app = new App();

    @Override
    public void start(Stage primaryStage) throws Exception {
        app.start(primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
