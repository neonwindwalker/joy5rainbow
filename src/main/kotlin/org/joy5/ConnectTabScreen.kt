package org.joy5

import javafx.application.Platform
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.ListView
import javafx.scene.control.TextField
import javafx.scene.input.MouseEvent
import javafx.scene.layout.GridPane
import javafx.scene.text.Text
import org.joy5.model.ComPortConnection
import org.joy5.model.Joy5Device
import org.joy5.model.MySettings

class ConnectTabScreen: GridPane {
    @FXML
    var statusText: Text? = null

    @FXML
    var textField: TextField? = null

    @FXML
    var button: Button? = null

    @FXML
    var portList: ListView<String>? = null

    constructor()

    init {
        org.joy5.utils.MyLoader.load(this)
        textField!!.text = MySettings.comPort

        Joy5Device.connectionStatusEvent.subscribe {
            Platform.runLater {
                updateStatus()
            }
        }

        rebuildComPortList()
        updateStatus()
    }

    @FXML
    fun onConnectClick(ev: ActionEvent?) {
        Joy5Device.connect(textField!!.text)
    }

    @FXML
    fun onListViewClick(ev: MouseEvent?) {
        textField!!.text = portList!!.selectionModel.selectedItem
    }

    fun rebuildComPortList() {
        val portList = portList ?: return
        portList.items.clear()
        Joy5Device.getAllComPorts().forEach {
            portList.items.add(it.systemPortName)
        }
    }

    fun updateStatus() {
        statusText?.text = if (Joy5Device.connectionStatus == ComPortConnection.State.Connected) {
            Joy5Device.connectionStatus.toString() + " " + Joy5Device.openedPort?.systemPortName
        } else Joy5Device.connectionStatus.toString()
    }
}