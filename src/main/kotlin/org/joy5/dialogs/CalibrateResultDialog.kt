package org.joy5.dialogs

import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog

class CalibrateResultDialog(str: String) : Dialog<ButtonType>() {
    init {
        title = "Calibration results"
        contentText = str
        val bt = ButtonType("OK")
        dialogPane.buttonTypes.add(bt)
        showAndWait()
        SaveAllSettingsToFlashDialog()
    }
}