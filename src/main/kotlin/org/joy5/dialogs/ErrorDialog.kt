package org.joy5.dialogs

import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog

class ErrorDialog : Dialog<ButtonType> {
    constructor(msg: String) {
        title = "Error"
        contentText = msg
        showAndWait()
    }

    constructor(th: Throwable) {
        title = "Error"
        contentText = th.message
        showAndWait()
    }

    init {
        val bt = ButtonType("OK")
        dialogPane.buttonTypes.add(bt)

    }
}