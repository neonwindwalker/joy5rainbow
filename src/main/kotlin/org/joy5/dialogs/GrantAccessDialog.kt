package org.joy5.dialogs

import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog


class GrantAccessDialog(val portFilePath: String) : Dialog<ButtonType>() {
    init {
        title = "Please grant access to port\n" + portFilePath
        contentText = "Permanent:\n"+
                "Temporal:\n" +
                 "\t sudo chmod 777 " + portFilePath
        val btPermanent = ButtonType("Permanent")
        val btTemporal = ButtonType("Temporal")
        val btCancel = ButtonType("Cancel")
        dialogPane.buttonTypes.add(btPermanent)
        dialogPane.buttonTypes.add(btTemporal)
        dialogPane.buttonTypes.add(btCancel)
        val res = showAndWait()
        if (res.isPresent) {
            if (res.get() == btTemporal) {
                Runtime.getRuntime().exec("konsole -e sudo chmod 777 " + portFilePath)
            } else if (res.get() == btPermanent) {
                //TODO
            }
        }
    }
}