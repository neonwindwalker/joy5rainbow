package org.joy5.dialogs

import javafx.application.Platform
import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog
import org.joy5.model.Joy5Device

class SaveAllSettingsToFlashDialog : Dialog<ButtonType>() {
    init {
        title = "Save All settings to device flash memory?"
        contentText = "without saving after unplug device all settings will be lost"
        val btSave = ButtonType("Save")
        val btCancel = ButtonType("Cancel")
        dialogPane.buttonTypes.add(btSave)
        dialogPane.buttonTypes.add(btCancel)

        val res = showAndWait()
        if (res.isPresent) {
            if (res.get() == btSave) {
                Joy5Device.addCmdSaveAllSettingsToFlash().subscribe({
                    if (it.isNotBlank())
                        Platform.runLater { org.joy5.dialogs.ErrorDialog(it) }
                }, {
                    Platform.runLater { org.joy5.dialogs.ErrorDialog(it) }
                })
            }
        }
    }
}