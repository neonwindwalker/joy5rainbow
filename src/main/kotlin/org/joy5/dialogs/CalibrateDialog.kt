package org.joy5.dialogs

import javafx.application.Platform
import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog
import org.joy5.model.Joy5Device

class CalibrateDialog : Dialog<ButtonType>() {
    val cmd = Joy5Device.CalibrateCmd()
    init {
        title = "Calibration in progress..."
        contentText = "Please rotate all sticks, and after click button"
        val btCancel = ButtonType("I rotate all stick")
        dialogPane.buttonTypes.add(btCancel)

        cmd.result.subscribe({
            Platform.runLater { CalibrateResultDialog(it) }
        }, {
            Platform.runLater { ErrorDialog(it) }
        })

        Joy5Device.addCmd(cmd)

        val res = showAndWait()
        cmd.finishFlag = true
        if (res.isPresent) {
            if (res.get() == btCancel) {
            }
        }
    }
}