package org.joy5

import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.layout.GridPane
import org.joy5.dialogs.CalibrateDialog
import org.joy5.utils.MyLoader

class SensorsTabScreen: GridPane {

    constructor()

    init {
        MyLoader.load(this)
    }

    @FXML
    fun onStartCalibrateButtonClick(ev: ActionEvent?) {
        CalibrateDialog()
    }
}