package org.joy5

import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import org.joy5.utils.MyLoader
import java.io.IOException

class MainWindow: TabPane() {
    @FXML
    var connectTab: Tab? = null

    @FXML
    var sensorsTab: Tab? = null

    @FXML
    var layoutsTab: Tab? = null

    init {
        MyLoader.load(this)
    }
}