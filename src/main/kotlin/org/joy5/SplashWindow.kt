package org.joy5

import javafx.application.Platform
import javafx.fxml.FXML
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.stage.Stage
import javafx.stage.StageStyle
import org.joy5.model.Joy5Device
import org.joy5.utils.MyLoader
import java.util.ArrayList


class SplashWindow: GridPane() {
    @FXML
    var stick1: SectorsView? = null
    @FXML
    var stick2: SectorsView? = null
    @FXML
    var stick3: SectorsView? = null
    @FXML
    var stick4: SectorsView? = null
    @FXML
    var stick5: SectorsView? = null

    val stage = Stage(StageStyle.TRANSPARENT)
    var myScene: Scene
    val sectors = ArrayList<SectorsView>()
    init {
        MyLoader.load(this)

        sectors.add(stick1!!)
        sectors.add(stick2!!)
        sectors.add(stick3!!)
        sectors.add(stick4!!)
        sectors.add(stick5!!)

        stick1!!.background = Background(BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY))
        stick2!!.background = Background(BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY))
        stick3!!.background = Background(BackgroundFill(Color.BLUE, CornerRadii.EMPTY, Insets.EMPTY))
        stick4!!.background = Background(BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY))
        stick5!!.background = Background(BackgroundFill(Color.CYAN, CornerRadii.EMPTY, Insets.EMPTY))

        myScene = Scene(this, 1000.0, 500.0)
        stage.setScene(myScene)

        Joy5Device.stateEvent.subscribe {
            Platform.runLater {
                update()
            }
        }
    }

    var firstActiveTime: Long? = null

    fun update() {
        val layouts = Joy5Device.layouts ?: return
        val state = Joy5Device.state ?: return
        val layout = layouts.getOrNull(Joy5Device.state?.layout ?: -1) ?: return

        val isActive = false //state.sticks.any { it.r > 15 } ?: false
        if (isActive) {
            if (firstActiveTime == null)
                firstActiveTime = System.currentTimeMillis()
        } else firstActiveTime = null

        if (firstActiveTime != null && System.currentTimeMillis() - firstActiveTime!! > 0) {
            for (i in sectors.indices) {
                val data = layout.getOrNull(i) ?: continue
                sectors[i].update(data.toTypedArray(), state.joysticks[i])
            }
            stage.show()
            stage.toFront()
        } else {
            stage.hide()
        }
    }
}