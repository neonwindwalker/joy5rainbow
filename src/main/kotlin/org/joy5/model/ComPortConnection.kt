package org.joy5.model

import com.fazecast.jSerialComm.SerialPort
import io.reactivex.rxjava3.subjects.PublishSubject
import javafx.application.Platform
import kotlinx.serialization.*
import kotlinx.serialization.json.*
import org.joy5.dialogs.ErrorDialog
import java.io.*
import java.util.concurrent.TimeoutException

open class ComPortConnection() {
    companion object {
        val DEFAULT_RESPONSE_AWAIT_MILIS = 5000L
    }

    fun getAllComPorts(): Array<SerialPort> {
        return SerialPort.getCommPorts()
    }

    fun getPortFilePath(p: SerialPort): String {
        return p.javaClass.getDeclaredField("comPort").let {
            it.isAccessible = true
            it.get(p) as String
        }
    }

    enum class State {
        Disconnected,
        Connecting,
        PortConnected,
        Connected,
    }

    @Volatile var openedPort: SerialPort? = null
        private set
    private var thread: Thread? = null
    @Volatile protected var stopThreadFlag: Boolean = false
        private set

    @Volatile var connectionStatus = State.Disconnected
        private set(v) {
            field = v
            connectionStatusEvent.onNext(v)
        }
    val connectionStatusEvent = PublishSubject.create<State>()

    fun connect(name: String) {
        try {
            val p = SerialPort.getCommPort(name)
            p.baudRate = 115200
            connect(p)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun connect(comPort: SerialPort) {
        disconnect()
        stopThreadFlag = false

        thread = Thread {
            try {
                threadFunc(comPort)
            } catch (ex: Throwable) {
                closePort()
            }
        }.apply {
            name = "ComPortIO"
        }
        thread?.start()
    }

    open fun disconnect() {
        stopThreadFlag = true
        thread?.join()
    }

    fun write(str: String) {
        write(str.toByteArray())
    }

    fun write(dat: ByteArray) {
        assert(Thread.currentThread() == thread)
        val port = openedPort ?: throw InvalidObjectException("openedPort == null")
        port.writeBytes(dat, dat.size.toLong())
    }

    fun read(): ByteArray {
        assert(Thread.currentThread() == thread)
        val port = openedPort ?: throw InvalidObjectException("openedPort == null")
        return ByteArray(port.bytesAvailable()).also {
            if (it.size > 0)
                port.readBytes(it, it.size.toLong())
        }
    }

    private val byteBuffer = ByteArray(1)

    fun readByte(): Byte? {
        assert(Thread.currentThread() == thread)
        val port = openedPort ?: throw InvalidObjectException("openedPort == null")
        if (port.readBytes(byteBuffer, 1) == 0)return null
        /*File("comlog.txt").apply {
            appendText(byteBuffer[0].toChar().toString())
        }*/
        return byteBuffer[0]
    }

    fun write(b: Byte) {
        assert(Thread.currentThread() == thread)
        byteBuffer[0] = b
        write(byteBuffer)
    }

    val bytesAvailable: Int
        get() = openedPort?.bytesAvailable() ?: 0

    open fun onNoAccessToComPort(file: String) {}
    open fun onPortConnected():Boolean { return true }
    open fun onDoPortIO() {}
    open fun onPrePortClose() {}
    open fun sleep() { Thread.sleep(20) }
    open fun onException(ex: Throwable) { ex.printStackTrace() }

    private fun threadFunc(comPort: SerialPort) {
        connectionStatus = State.Connecting
        if (!comPort.openPort()) {
            connectionStatus = State.Disconnected
            onNoAccessToComPort(getPortFilePath(comPort))
            return
        }

        openedPort = comPort
        connectionStatus = State.PortConnected

        try {
            if (onPortConnected()) {
                connectionStatus = State.Connected
            } else {
                closePort()
                return
            }
        } catch (ex: Exception) {
            onException(ex)
            closePort()
            return
        }

        val port = comPort
        while (!stopThreadFlag) {
            try {
                onDoPortIO()
            } catch (ex: Exception) {
                stopThreadFlag = true
                onException(ex)
                break
            }

            if (port.bytesAvailable() == 0) {
                sleep()
            }
        }

        closePort()
    }

    private fun closePort() {
        openedPort ?: return

        try {
            onPrePortClose()
        } catch (ex: Exception) {
            onException(ex)
        }

        openedPort?.closePort()
        openedPort = null
        connectionStatus = State.Disconnected
    }
}