package org.joy5.model

import java.util.prefs.Preferences

object MySettings {
    val pref = Preferences.userNodeForPackage(javaClass)

    var comPort: String
        get() = pref.get("comPort", "")
        set(v) { pref.put("comPort", v)}
}