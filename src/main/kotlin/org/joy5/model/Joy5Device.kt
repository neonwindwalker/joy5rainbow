package org.joy5.model

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject
import javafx.application.Platform
import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import org.joy5.MCAbout
import org.joy5.MCLayoutSector
import org.joy5.MCState
import org.joy5.dialogs.ErrorDialog
import org.joy5.dialogs.GrantAccessDialog
import java.lang.Exception

object Joy5Device : ComPortProtocolDomestication() {

    var layouts: ArrayList<ArrayList<ArrayList<Int>>>? = null
        private set

    val stateEvent = PublishSubject.create<MCState>()
    var state: MCState? = null
        private set(v) {
            field = v
            stateEvent.onNext(v)
        }

    override fun onPortConnected(): Boolean {
        if (!super.onPortConnected()) return false

        val about = runJCmd<MCAbout>("about")
        if (!about.device.contains("Joy5"))return false

        layouts = runJCmd<ArrayList<ArrayList<ArrayList<Int>>>>("print /layouts")

        MySettings.comPort = openedPort?.systemPortName ?: ""

        return true
    }

    override fun onDoPortIO() {
        if (cmdList.isEmpty()) {
            state = runJCmd<MCState>("watch_state 2")
        } else receiveBells()

        super.onDoPortIO()
    }

    override fun onBell(data: String) {
        try {
            if (data.startsWith("state:")) {
                state = json.decodeFromString<MCState>(data.substring("state:".length))
            }
        } catch (ex: Exception) {
            onException(ex)
        }
    }

    override fun onException(ex: Throwable) {
        Platform.runLater { ErrorDialog(ex) }
    }

    override fun onNoAccessToComPort(file: String) {
        Platform.runLater {
            GrantAccessDialog(file)
        }
    }

    fun addCmdSaveAllSettingsToFlash() : Observable<String> {
        return addCmd("save_all_settings_to_flash")
    }

    class CalibrateCmd : ICmd, ITerminatableCmd {
        var finishFlag = false
        var terminateFlag = false
        val result = PublishSubject.create<String>()

        override fun io(dev: ComPortProtocolDomestication) {
            dev.sendCmdRun("calibrate")
            while(!finishFlag && !terminateFlag)dev.sleep()
            if (terminateFlag)
                dev.sendControlC()
            else
                dev.write("\r")
            dev.sendCmdEnd()
            dev.receiveResponseBegin()
            val str = dev.receiveResponse()
            result.onNext(str)
            result.onComplete()
        }

        override fun terminate() {
            terminateFlag = true
        }
    }
}