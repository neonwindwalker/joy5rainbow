package org.joy5.model

import io.reactivex.rxjava3.subjects.PublishSubject
import io.reactivex.rxjava3.subjects.Subject
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.joy5.MCState

open class ComPortProtocolDomestication : ComPortProtocol() {
    val json = Json {
        ignoreUnknownKeys = true
    }

    fun runCmd(cmd: String): String {
        sendCmdRun(cmd)
        sendCmdEnd()
        receiveResponseBegin()
        return receiveResponse()
    }

    fun sendControlC() {
        write(3.toByte())
    }

    inline fun<reified T> runJCmd(cmd: String): T {
        val str = runCmd(cmd)
        val obj = json.decodeFromString<T>(str)
        return obj
    }

    interface ICmd {
        fun io(dev: ComPortProtocolDomestication)
    }

    interface ITerminatableCmd {
        fun terminate()
    }

    protected val cmdList = ArrayList<ICmd>()
    @Volatile var currentCmd: ICmd? = null
        private set

    fun addCmd(cmd: ICmd) {
        synchronized(cmdList) {
            cmdList.add(cmd)
        }
    }

    override fun onDoPortIO() {
        synchronized(cmdList) {
            if (cmdList.isNotEmpty()) {
                currentCmd = cmdList[0]
                cmdList.removeAt(0)
            }
        }

        currentCmd?.let {
            it.io(this)
            currentCmd = null
        }
    }

    override fun onPrePortClose() {
        synchronized(cmdList) {
            cmdList.clear()
            (currentCmd as? ITerminatableCmd)?.terminate()
            currentCmd = null
        }
    }

    class SimpleCmd(val cmd: String) : ICmd {
        val result = PublishSubject.create<String>()

        override fun io(dev: ComPortProtocolDomestication) {
            val resp = dev.runCmd(cmd)
            result.onNext(resp)
            result.onComplete()
        }
    }

    fun addCmd(cmd: String): Subject<String> {
        val entry = SimpleCmd(cmd)
        addCmd(entry)
        return entry.result
    }
}