package org.joy5.model

import java.io.IOException
import java.util.concurrent.ExecutionException
import java.util.concurrent.RejectedExecutionException
import java.util.concurrent.TimeoutException

open class ComPortProtocol : ComPortConnection() {
    companion object {
        val Ascii_SOH: Byte = 1 //start response, start request
        val Ascii_EOT: Byte = 4 //end response, end request
        val Ascii_BEL: Byte = 7 //start bell data
        val Ascii_ETB: Byte = 23 //end bell data
    }

    fun readBlockedUntil(ch: Byte, timeout_milisec: Long = DEFAULT_RESPONSE_AWAIT_MILIS): ByteArray {
        var buff = ByteArray(0)
        val startAwaitTime = System.currentTimeMillis()
        while (!stopThreadFlag) {
            readByte()?.let { b->
                if (b == ch)return buff
                buff += b
            } ?: run {
                val dt = System.currentTimeMillis() - startAwaitTime
                if (dt > timeout_milisec)
                    throw TimeoutException("cant receive $ch, await $dt milisec")
                sleep()
            }
        }
        throw RejectedExecutionException("stopThreadFlag == true before reading $ch")
    }

    fun sendCmdRun(cmd: String) {
        if (isInCmdQuotes)
            throw IllegalAccessException("cant call second cmd sendCmdRun before first cmd receiveResponse")
        isInCmdQuotes = true
        write(Ascii_SOH.toChar() + cmd + "\r")
    }

    //can write custom bytes before sendCmdEnd()

    fun sendCmdEnd() {
        write(Ascii_EOT)
    }

    private var beforeResponseData = ByteArray(0)

    open fun onBell(data: String) {}

    private fun handleBeforeResponseData() {
        var bellData: ByteArray? = null
        var trimPosition: Int? = null
        beforeResponseData.forEachIndexed() { index, b->
            when(b) {
                Ascii_BEL -> {
                    bellData = ByteArray(0)
                }
                Ascii_ETB -> {
                    bellData?.let { onBell(it.decodeToString()) }
                    bellData = null
                    trimPosition = index + 1
                }
                else -> {
                    if (bellData != null) {
                        bellData = bellData!! + b
                    }
                }
            }
        }
        trimPosition?.let {
            beforeResponseData = if (it <= beforeResponseData.lastIndex)
                beforeResponseData.slice(it..beforeResponseData.lastIndex).toByteArray()
            else
                ByteArray(0)
        }
    }

    private var isInCmdQuotes = false

    fun receiveBells() { //dont call in [sendCmdRun .. receiveResponse]
        if (isInCmdQuotes)
            throw IllegalAccessException("cant call in [sendCmdRun .. receiveResponse]")
        beforeResponseData += read()
        handleBeforeResponseData()
    }

    fun receiveResponseBegin() { //return bells before
        beforeResponseData += readBlockedUntil(Ascii_SOH)
        handleBeforeResponseData()
        beforeResponseData = ByteArray(0)
    }

    //can read custom bytes before receiveResponseEnd()

    fun receiveResponse(): String {
        val str = readBlockedUntil(Ascii_EOT).decodeToString()
        isInCmdQuotes = false
        return str
    }
}