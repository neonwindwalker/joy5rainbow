package org.joy5

import javafx.geometry.Point2D
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

@Serializable
data class MCAbout(val device: String, val fw_version: Int)

@Serializable
class MCState(val layout: Int, val needShowHelp: Boolean) {
    @Serializable
    data class SectorAndLayout(val sector: Int, val layout: Int)

    @Serializable
    @SerialName("JoystickState")
    open class JoystickState {
        val pushedButton: SectorAndLayout?=null
        val entredSector: Int?=null
    }

    @Serializable
    @SerialName("JoystickRFiState")
    class JoystickRFiState : JoystickState() {
        val r1000: Int?=null
        val fi3600: Int?=null
    }

    val joysticks = emptyArray<JoystickState>()
}

@Serializable
class MCLayoutSector(
    var center_angle_3600: Int,
    var half_size_angle_3600: Int,
    var keyCode: Long)

@Serializable
class MCVector2i(var x: Int, var y: Int)