package org.joy5

import javafx.application.Platform
import javafx.scene.Scene
import javafx.scene.paint.Color
import javafx.stage.Stage
import javafx.stage.StageStyle
import kotlinx.serialization.decodeFromString
import org.joy5.dialogs.GrantAccessDialog
import org.joy5.model.Joy5Device
import org.joy5.model.MySettings
import java.awt.*
import java.awt.event.ActionEvent
import java.io.IOException
import javax.imageio.ImageIO
import javax.swing.SwingUtilities

class App {
    var stage: Stage? = null
    var splashWindow: SplashWindow? = null

    fun start(primaryStage: Stage?) {
        /*Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();*/
        stage = primaryStage

        // instructs the javafx system not to exit implicitly when the last application window is shut.
        Platform.setImplicitExit(false)

        // sets up the tray icon (using awt code run on the swing thread).
        SwingUtilities.invokeLater { addAppToTray() }

        // out stage will be translucent, so give it a transparent style.
        stage!!.initStyle(StageStyle.TRANSPARENT)

        /*
        // create the layout for the javafx stage.
        StackPane layout = new StackPane();
        layout.setStyle(
                "-fx-background-color: rgba(255, 255, 255, 0.5);"
        );
        layout.setPrefSize(300, 200);

        String imageUrl = "/icon.jpg";
        Image image = new Image(imageUrl,160,60,false,true);

        // Create the ImageView
        ImageView imageView = new ImageView(image);
        layout.getChildren().add(imageView);

        // this dummy app just hides itself when the app screen is clicked.
        // a real app might have some interactive UI and a separate icon which hides the app window.
        layout.setOnMouseClicked(event -> stage.hide());

        // a scene with a transparent fill is necessary to implement the translucent app window.
        Scene scene = new Scene(layout);

         */
        val root = org.joy5.MainWindow()
        val scene = Scene(root)
        scene.fill = Color.TRANSPARENT
        stage!!.scene = scene

        if (MySettings.comPort.isNotEmpty()) {
            Joy5Device.connect(MySettings.comPort)
        }

        splashWindow = SplashWindow()


    }

    private fun addAppToTray() {
        try {
            // ensure awt toolkit is initialized.
            Toolkit.getDefaultToolkit()

            // app requires system tray support, just exit if there is no support.
            if (!SystemTray.isSupported()) {
                println("No system tray support, application exiting.")
                Platform.exit()
            }

            // set up a system tray icon.
            val tray = SystemTray.getSystemTray()
            val imageLoc = javaClass.getResource("/icon.jpg")
            val image: Image = ImageIO.read(imageLoc)
            val trayIcon = TrayIcon(image)

            // if the user double-clicks on the tray icon, show the main app stage.
            trayIcon.addActionListener { event: ActionEvent? ->
                Platform.runLater { showStage() }
            }

            // if the user selects the default menu item (which includes the app name),
            // show the main app stage.
            val openItem = MenuItem("hello, world")
            openItem.addActionListener { event: ActionEvent? ->
                Platform.runLater { showStage() }
            }

            // the convention for tray icons seems to be to set the default icon for opening
            // the application stage in a bold font.
            val defaultFont = Font.decode(null)
            val boldFont = defaultFont.deriveFont(Font.BOLD)
            openItem.font = boldFont

            // to really exit the application, the user must go to the system tray icon
            // and select the exit option, this will shutdown JavaFX and remove the
            // tray icon (removing the tray icon will also shut down AWT).
            val exitItem = MenuItem("Exit")
            exitItem.addActionListener { event: ActionEvent? ->
                Platform.exit()
                tray.remove(trayIcon)
            }

            // setup the popup menu for the application.
            val popup = PopupMenu()
            popup.add(openItem)
            popup.addSeparator()
            popup.add(exitItem)
            trayIcon.popupMenu = popup

            // add the application tray icon to the system tray.
            tray.add(trayIcon)
        } catch (e: AWTException) {
            println("Unable to init system tray")
            e.printStackTrace()
        } catch (e: IOException) {
            println("Unable to init system tray")
            e.printStackTrace()
        }
    }

    fun showStage() {
        if (stage != null) {
            stage!!.show()
            stage!!.toFront()
        }
    }
}