package org.joy5.utils

import javafx.fxml.FXMLLoader
import javafx.scene.Node
import java.io.IOException

object MyLoader {
    fun load(obj: Node) {
        val cl = obj.javaClass
        val fxmlLoader = FXMLLoader(cl.getResource("/" + cl.simpleName + ".fxml"))
        fxmlLoader.setRoot(obj)
        fxmlLoader.setController(obj)
        try {
            fxmlLoader.load<Any>()
        } catch (exception: IOException) {
            throw RuntimeException(exception)
        }
    }
}