package org.joy5

import javafx.geometry.Insets
import javafx.scene.control.TabPane
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.CornerRadii
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import javafx.scene.text.Text
import org.joy5.utils.MyLoader


class SectorsView: TabPane() {
    val texts = Array(10) {
        Text().apply {
        }
    }
    val point = Circle().apply {
        radius = 10.0
        background = Background(BackgroundFill(Color(0.0, 0.0, 0.0, 0.2), CornerRadii.EMPTY, Insets.EMPTY))
    }

    init {
        children.addAll(texts)
        children.add(point)
    }

    fun update(elements: Array<Int>, stick: MCState.JoystickState) {
        val R = Math.min(width, height) / 3
        for (i in texts.indices) {
            val t = texts[i]
            if (i>= elements.size) {
                t.isVisible = false
                continue
            }
            val e = elements[i]

            /*
            val fi = e.center_angle_3600.toDouble() * 2 * Math.PI / 3600

            t.text = e.keyCode.toString()
            t.layoutX = width/2 + Math.cos(fi) * R - t.layoutBounds.width/2
            t.layoutY = height/2 - Math.sin(fi) * R + t.layoutBounds.height/2
            t.fill = Color.BLACK //if (stick.e == i)Color.RED else

             */
        }

        /*
        val fi = stick.fi.toDouble() * 2 * Math.PI / 3600
        val r = stick.r.toDouble() / 100 * R
        point.centerX = width/2 + Math.cos(fi) * r
        point.centerY = height/2 - Math.sin(fi) * r
         */
    }
}